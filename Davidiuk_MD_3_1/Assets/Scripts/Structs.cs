﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlockSpace
{
    public struct Block
    {
        public GameObject obj;
        public Vector3 pos;
        public float distance;
    }
}
