﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public GameObject ball;
    public GameObject manager;
    public InputMaster controls;
    public float speed = 0.2f;


    private MySceneManager myManager;
    private Rigidbody playerRB;
    private Vector3 direction;

    [Tooltip("Use this check box if it's a first player")]
    public bool isFirstPlayer;

    private BallController ballMover;

    public void OnEnable()
    {
        controls.Enable();
    }

    public void Awake()
    {
        if (isFirstPlayer)
        {
            controls = new InputMaster();
            controls.Player1.Shoot.performed += _ => Shoot();
            controls.Player1.Movement.started += ctx => Move((Vector3)ctx.ReadValue<Vector2>());
            controls.Player1.Movement.canceled += ctx => Move(Vector3.zero);
            controls.Player1.Pause.performed += _ => myManager.OpenPauseMenu();
        }
        else
        {
            controls = new InputMaster();
            controls.Player2.Shoot.performed += _ => Shoot();
            controls.Player2.Movement.started += ctx => Move((Vector3)ctx.ReadValue<Vector2>());
            controls.Player2.Movement.canceled += ctx => Move(Vector3.zero);
        }
    }

    void Start()
    {
        myManager = manager.GetComponent<MySceneManager>();
        ballMover = ball.GetComponent<BallController>();
        playerRB = GetComponent<Rigidbody>();
    }

    void Update()
    {
        playerRB.AddForce(direction * speed , ForceMode.Impulse);
    }

    void Shoot()
    {
        ballMover.BallShoot();
    }

    void Move(Vector3 direct)
    {
        direction = new Vector3(direct.x, 0, -direct.y);
    }

    public void OnDisable()
    {
        controls.Disable();
    }
}
