﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public GameObject settingsMenu;
    public GameObject manager;

    private float menuSizeS = 0;
    private RectTransform rectS;
    private MySceneManager myManager;
    void Start()
    {
        myManager = manager.GetComponent<MySceneManager>();
        rectS = settingsMenu.GetComponent<RectTransform>();
        rectS.localScale = new Vector3(0, 0, 0);
    }

    void Update()
    {
        rectS.localScale = new Vector3(menuSizeS, menuSizeS, menuSizeS);
    }


    #region Voids
    public void OpenSettingsMenu()
    {
        StartCoroutine(OpenSettingsMenu_Cor());
        StartCoroutine(myManager.ClosePauseMenu_Cor());
    }
    public void CloseSettingsMenu()
    {
        StartCoroutine(CloseSettingsMenu_Cor());
        StartCoroutine(myManager.OpenPauseMenu_Cor());
    }

    

    public void ReStart()
    {
        SceneManager.LoadScene("GameScene");
    }
    public void Exit()
    {
        EditorApplication.isPlaying = false;
    }

    #endregion

    #region IEnumerator
    
    public IEnumerator OpenSettingsMenu_Cor()
    {
        while (menuSizeS < 1)
        {
            menuSizeS += 0.1f;
            yield return new WaitForSeconds(0.015f);
        }
        //Time.timeScale = 0.1f;
    }

    public IEnumerator CloseSettingsMenu_Cor()
    {
        while (menuSizeS > 0)
        {
            menuSizeS -= 0.1f;
            yield return new WaitForSeconds(0.015f);
        }
        menuSizeS = 0;
        //Time.timeScale = 1f;
    }
    #endregion

}
