﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// немного некоректно работает. лучше всего при количестве блоков = 7

public class BlockCreator : MonoBehaviour
{
    public int blockCount=7;
    public GameObject block;
    void Start()
    {
        for(int i = 0; i < blockCount; i++)
        {
            Vector3 pos = new Vector3(Random.Range(-3, 3), Random.Range(12, 18), Random.Range(-3, 3));
            GameObject clone = Instantiate(block, pos, Random.rotation);
        }
    }
}
