﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    public GameObject Player1;
    public GameObject Player2;
    public float startSpeed = 1f;
    public float Maxspeed   = 5f;
    public GameObject manager;

    private float speed;
    private Rigidbody rb;
    private MySceneManager Health;
    private Vector3 lastVelocity;
    private Vector3 goalVelocity = new Vector3(0,0,0);
    private MySceneManager myManager;

    private void Start()
    {
        myManager = manager.GetComponent<MySceneManager>();
        Health = FindObjectOfType<MySceneManager>().GetComponent<MySceneManager>();
        rb     = GetComponent<Rigidbody>();
        speed  = startSpeed;
        transform.position = Player1.transform.position + Player1.transform.forward;
        rb.constraints     = RigidbodyConstraints.FreezePosition;
        transform.rotation = Player1.transform.rotation;
        transform.SetParent(Player1.transform);
    }

    void FixedUpdate()
    {
        if (startSpeed > Maxspeed) speed = Maxspeed;
        lastVelocity = rb.velocity;
        rb.velocity  = goalVelocity * speed;
    }

    void OnCollisionEnter(Collision collider)
    {
            ContactPoint cp = collider.contacts[0];
            goalVelocity = Vector3.Reflect(lastVelocity, cp.normal);

        if (collider.gameObject.CompareTag("Goal_1"))
        {
            Goal(Player1);
        }
        if (collider.gameObject.CompareTag("Goal_2"))
        {
            Goal(Player2);
        }
        if (collider.gameObject.CompareTag("Block"))
        {
            Destroy(collider.gameObject);
            speed += 1f;
        }
        if (collider.gameObject.CompareTag("Wall"))
        {
            return;
        }
    }

    public void Goal(GameObject player)
    {
        speed = startSpeed;
        transform.position = player.transform.position + player.transform.forward;
        rb.constraints     = RigidbodyConstraints.FreezePosition;
        transform.rotation = player.transform.rotation;
        transform.SetParent(player.transform);
        if(player == Player1)
        {
            Health.HealthCount_1--;
            Debug.Log("Health count for the first player: "  + Health.HealthCount_1);
        }
        else
        {
            Health.HealthCount_2--;
            Debug.Log("Health count for the second player: " + Health.HealthCount_2);
        }
    }
    public void BallShoot()
    {
        goalVelocity   = new Vector3(0,1,0.05f)*startSpeed;
        rb.constraints = RigidbodyConstraints.None;
        transform.SetParent(null);
    }
}
