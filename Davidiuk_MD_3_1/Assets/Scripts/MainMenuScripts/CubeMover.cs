﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeMover : MonoBehaviour
{
    [SerializeField]
    private GameObject[] Blocks = new GameObject[74];
    private float[] distances = new float[74];
    private Vector3[] positions = new Vector3[74];

    float t;
    void Start()
    {

        Blocks = GameObject.FindGameObjectsWithTag("Block");
        for (int i = 0; i < Blocks.Length; i++)
        {
            positions[i] = Blocks[i].transform.position;
            distances[i] = Random.Range(0.1f, 1.5f);
        }
    }

    void Update()
    {
        t += Time.deltaTime;
        for (int i = 0; i < Blocks.Length; i++)
        {
            Blocks[i].transform.position = positions[i] + transform.forward * Mathf.Sin(t) * distances[i];
        }
    }
}
