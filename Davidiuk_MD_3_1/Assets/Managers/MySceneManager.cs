﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class MySceneManager : MonoBehaviour
{
    public int HealthCount_1 = 3;
    public int HealthCount_2 = 3;

    public GameObject pauseMenu;

    //[HideInInspector]
    public byte gameTime;

    private float widht;
    private float height;

    private float menuSize = 0;
    private RectTransform rect;
    void Start()
    {
        rect = pauseMenu.GetComponent<RectTransform>();
        rect.localScale = new Vector3(0, 0, 0);
        //pauseMenu.SetActive(false);
    }

    void Update()
    {
        HealthChecking();
        rect.localScale = new Vector3(menuSize, menuSize, menuSize);
    }

    void HealthChecking()
    {
        if (HealthCount_1 == 0 || HealthCount_2 == 0)
        {
            EditorApplication.isPlaying = false;
        }
    }

    public void OpenPauseMenu()
    {
        StartCoroutine(OpenPauseMenu_Cor());
    }

    public void ClosePauseMenu()
    {
        StartCoroutine(ClosePauseMenu_Cor());
    }

    public IEnumerator ClosePauseMenu_Cor()
    {
        while (menuSize > 0)
        {
            menuSize -= 0.1f;
            yield return new WaitForSeconds(0.015f);
        }
        menuSize = 0;
        //Time.timeScale = 1f;
    }

    public IEnumerator OpenPauseMenu_Cor()
    {
        while (menuSize < 1)
        {
            menuSize += 0.1f;
            yield return new WaitForSeconds(0.015f);
        }
        //Time.timeScale = 0.1f;
    }

}
