﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;
using System.Threading;

public class SavedataManager : MonoBehaviour
{
    [TextArea, SerializeField]
    private string mainData = ("Translate = 2;\nScore = 100;");
    public bool RewriteData = false;

    [Range(0,1)]
    public int languageData;
    [Range(0, 10)]
    public int numOfCompletedLvlsData;
    public int scoreData;

    string path = Environment.CurrentDirectory;

    public void Awake()
    {
        DontDestroyOnLoad(this);

        if (FindObjectsOfType(GetType()).Length > 1)
            Destroy(gameObject);
    }

    //#################(Start)################//
    void Start()
    {
        if (File.Exists(path + "\\SaveDataFile.txt"))
        {
            languageData = GettingData("Language");
            numOfCompletedLvlsData = GettingData("NumOfCompletedLvls");
            scoreData = GettingData("Score");
        }
        else
        {
            var SaveDataFile = File.Create(path + "\\SaveDataFile.txt");
            SaveDataFile.Close();

            using (var stream = File.OpenWrite(path + "\\SaveDataFile.txt"))
            {
                using (BinaryWriter writer = new BinaryWriter(stream, System.Text.Encoding.UTF8, false))
                {
                    writer.Write(mainData);
                }
            }
        }
    }

    //#################(Update)################//
    void Update()
    {
        
    }

    //#################(Voids)################//
    #region Voids

    private void OnValidate()
    {
        if (RewriteData)
        {
            using (var stream = File.OpenWrite(path + "\\SaveDataFile.txt"))
            {
                using (BinaryWriter writer = new BinaryWriter(stream, System.Text.Encoding.UTF8, false))
                {
                    writer.Write(mainData);
                }
            }

            RewriteData = false;
        }
    }

    public int GettingData(string data)
    {
        using (var readerStream = File.OpenRead(path + "\\SaveDataFile.txt"))
        {
            using (var reader = new BinaryReader(readerStream))
            {
                var myString = reader.ReadString();
                myString.Replace(" ", "");
                myString = myString.Substring(myString.IndexOf(data));
                myString = myString.Substring(myString.IndexOf("=")+1);
                myString = myString.Substring(0, myString.IndexOf(';'));
                return Convert.ToInt32(myString);
            }
        }
    }

    #endregion
}
