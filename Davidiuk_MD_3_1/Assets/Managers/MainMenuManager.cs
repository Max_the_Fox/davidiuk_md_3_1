﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour
{
    public GameObject cam;


    void Start()
    {
        cam = Camera.main.gameObject;
    }

    void Update()
    {
        
    }

    public void NewGame()
    {
        SceneManager.LoadScene("FirstLevel");
    }

    public void Exit()
    {
        EditorApplication.isPlaying = false;
        Application.Quit();
    }

    public void ToLevels()
    {
        StartCoroutine(test());
    }

    public void OutOfLevels()
    {

    }

    public IEnumerator test()
    {
        cam.transform.position = Vector3.Lerp(cam.transform.position, Time.deltaTime)
        yield return null;
    }
}
